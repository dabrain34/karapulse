use std::{
    fs::File,
    io::Read,
    path::{Path, PathBuf},
    sync::{Arc, Mutex},
};

use anyhow::Context;
use log::info;

#[derive(Clone, Debug)]
pub struct Settings {
    pub path: PathBuf,
    inner: Arc<Mutex<Inner>>,
}

#[derive(Clone, Default, Debug, serde::Deserialize, serde::Serialize)]
pub struct Inner {
    #[serde(default)]
    pub admin: Admin,
}

#[derive(Clone, Debug, Default, serde::Deserialize, serde::Serialize)]
pub struct Admin {
    #[serde(skip_serializing_if = "Option::is_none")]
    /// secret used to sign JWT token, not exposed to client
    jwt_secret: Option<String>,
    /// admin password
    #[serde(default)]
    pub password: String,
}

fn generate_admin_jwt_secret() -> String {
    // generate a random secret
    use rand::distributions::{Alphanumeric, DistString};
    Alphanumeric.sample_string(&mut rand::thread_rng(), 32)
}

impl Settings {
    pub fn from_path(path: &Path) -> anyhow::Result<Self> {
        if !path.exists() {
            info!("generating configuration {}", path.display());
            // create an empty file and rely on serde defaults
            std::fs::File::create(path)?;
        } else {
            info!("reading configuration {}", path.display());
        }

        let mut cfg_file =
            File::open(path).context(format!("Opening configuration file: {}", path.display()))?;
        let mut contents = String::new();
        cfg_file
            .read_to_string(&mut contents)
            .context("Reading configuration file")?;

        let mut inner = toml::from_str::<Inner>(&contents).context("Parsing configuration file")?;
        if inner.admin.jwt_secret.is_none() {
            inner.admin.jwt_secret = Some(generate_admin_jwt_secret());
        }

        let settings = Self {
            path: path.to_path_buf(),
            inner: Arc::new(Mutex::new(inner)),
        };

        // generate the file or update it with new keys
        settings.write()?;

        Ok(settings)
    }

    /// only used for tests
    #[cfg(test)]
    pub fn new_test(path: &Path) -> Self {
        let inner = Inner {
            admin: Admin {
                jwt_secret: Some("secret".to_string()),
                password: "test-password".to_string(),
            },
        };

        Self {
            path: path.to_path_buf(),
            inner: Arc::new(Mutex::new(inner)),
        }
    }

    pub fn write(&self) -> anyhow::Result<()> {
        use std::io::prelude::*;
        let mut file = std::fs::File::create(&self.path)?;
        {
            let inner = self.inner.lock().unwrap();
            file.write_all(toml::to_string_pretty(&*inner).unwrap().as_bytes())?;
        }

        Ok(())
    }

    pub fn export(&self) -> Inner {
        let mut settings = {
            let inner = self.inner.lock().unwrap();
            inner.clone()
        };

        // do not expose the JWT secret to clients
        settings.admin.jwt_secret = None;

        settings
    }

    // return if JWT token was updated
    pub fn update(&self, settings: Inner) -> bool {
        let jwt_updated = {
            let mut inner = self.inner.lock().unwrap();
            // JWT secret cannot be updated by clients
            let jwt_secret = inner.admin.jwt_secret.clone();
            let password = inner.admin.password.clone();

            *inner = settings;

            if inner.admin.password == password {
                inner.admin.jwt_secret = jwt_secret;
                false
            } else {
                // admin password changed, regenerate JWT secret to invalidate existing tokens
                info!("regenerate JWT secret");
                inner.admin.jwt_secret = Some(generate_admin_jwt_secret());
                true
            }
        };

        self.write().unwrap();

        jwt_updated
    }

    pub fn admin_set_password(&self, password: String) {
        let mut inner = { self.inner.lock().unwrap().clone() };
        inner.admin.password = password;

        self.update(inner);
    }

    pub fn admin_jwt_secret(&self) -> String {
        let inner = self.inner.lock().unwrap();
        inner.admin.jwt_secret.clone().unwrap()
    }

    pub fn admin_check_password(&self, password: &str) -> bool {
        let inner = self.inner.lock().unwrap();
        inner.admin.password == password
    }
}
