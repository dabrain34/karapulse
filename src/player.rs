// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use crate::config;
use crate::karapulse::{Message, Song};
use anyhow::{anyhow, Result};
use get_if_addrs::get_if_addrs;
use gettextrs::*;
use gstreamer as gst;
use gstreamer::glib::{Sender, SourceId};
use gstreamer::prelude::*;
use std::cell::RefCell;
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex};
use url::Url;

use gtk::gdk;

pub struct Player {
    playbin: gst::Element,
    tx: Sender<Message>,
    announce_id: Arc<Mutex<RefCell<Option<SourceId>>>>,
    _bus_watch_guard: gst::bus::BusWatchGuard,

    pub picture: gtk::Picture,
}

fn get_web_urls() -> Vec<String> {
    // Ignore loopback and virtual interfaces
    // FIXME: don't hardcode port
    get_if_addrs()
        .unwrap()
        .iter()
        .filter(|iface| !iface.name.starts_with("virbr"))
        .filter(|iface| !iface.name.starts_with("vmnet"))
        .filter(|iface| !iface.name.starts_with("wg"))
        .filter(|iface| !iface.name.starts_with("docker"))
        .filter(|iface| !iface.name.starts_with("br-"))
        .map(get_if_addrs::Interface::ip)
        .filter(|ip| !ip.is_loopback())
        .filter(|ip| ip.is_ipv4())
        .map(|ip| format!("http://{}:1848", ip))
        .collect::<Vec<String>>()
}

fn get_overlay_text() -> String {
    get_web_urls().join("\n")
}

impl Player {
    pub fn new(tx: Sender<Message>) -> Result<Player> {
        let picture = gtk::Picture::new();
        picture.set_content_fit(gtk::ContentFit::Fill);

        let gtksink = gst::ElementFactory::make("gtk4paintablesink")
            .build()
            .unwrap();

        let paintable = gtksink.property::<gdk::Paintable>("paintable");

        let video_sink = if paintable
            .property::<Option<gdk::GLContext>>("gl-context")
            .is_some()
        {
            gst::ElementFactory::make("glsinkbin")
                .property("sink", &gtksink)
                .build()
                .unwrap()
        } else {
            gtksink
        };

        let playbin = gst::ElementFactory::make("playbin3")
            .build()
            .map_err(|_| anyhow!("Missing element playbin3"))?;

        playbin.set_property("video-sink", &video_sink);

        let spotify_cache = crate::spotify::cache_dir()?;

        playbin.connect("source-setup", false, move |args| {
            let source = args[1].get::<gst::Element>().unwrap();
            if source.type_().name() == "GstSpotifyBin" {
                // configure spotify source

                // FIXME: get those settings from config
                let username = std::env::var("LIBRESPOST_USER").unwrap();
                let password = std::env::var("LIBRESPOST_PASSWORD").unwrap();
                source.set_property("username", username);
                source.set_property("password", password);
                let cache = spotify_cache.to_str().unwrap();
                source.set_property("cache-credentials", cache);
                source.set_property("cache-files", cache);
            }

            None
        });

        playbin.connect("element-setup", false, |args| {
            let element = args[1].get::<gst::Element>().unwrap();
            // improve rendering of Spotify lyrics
            if element.type_().name() == "GstTextOverlay"
                && element.name() != "message"
                && element.name() != "ip"
            {
                element.set_property("draw-outline", false);
                element.set_property("draw-shadow", false);

                element.set_property_from_str("valignment", "top");
                element.set_property("ypad", 150i32);

                // subtitleoverlay will change this back
                element.connect_notify(Some("valignment"), |element, _pspec| {
                    let v = element.property_value("valignment");
                    let (_, v) = gstreamer::glib::EnumValue::from_value(&v).unwrap();
                    if v.nick() != "top" {
                        element.set_property_from_str("valignment", "top");
                    }
                });
            }

            None
        });

        picture.set_paintable(Some(&paintable));

        let bus_watch_guard = {
            let bus = playbin.bus().unwrap();
            let mut last_failed_uri = None;
            let tx_clone = tx.clone();

            bus.add_watch_local(move |_, msg| {
                match msg.view() {
                    gst::MessageView::Eos(..) => {
                        debug!("Reached EOS");
                        tx_clone.send(Message::PlayingDone).unwrap();
                    }
                    gst::MessageView::Error(err) => {
                        error!(
                            "Error from {:?}: {} ({:?})",
                            err.src().map(|s| s.path_string()),
                            err.error(),
                            err.debug()
                        );

                        // check if this URI has already failed so we don't skip songs multiple times
                        let source_uri = err
                            .src()
                            .and_then(get_pipeline)
                            .map(|pipeline| pipeline.property::<String>("uri"));

                        if source_uri == last_failed_uri {
                            return gstreamer::glib::ControlFlow::Continue;
                        }

                        if source_uri.is_some() {
                            last_failed_uri = source_uri;
                        }

                        tx_clone.send(Message::PlayingDone).unwrap();
                    }
                    _ => (),
                };

                gstreamer::glib::ControlFlow::Continue
            })
            .expect("Failed to add bus watch")
        };

        let player = Player {
            playbin,
            tx,
            announce_id: Arc::new(Mutex::new(RefCell::new(None))),
            _bus_watch_guard: bus_watch_guard,
            picture,
        };

        Ok(player)
    }

    fn start_pipeline(&self, source: Source, video_filter: &str) -> Result<()> {
        self.playbin.set_state(gst::State::Null)?;

        match source {
            Source::Path(path) => self.set_source_path(&path)?,
            Source::Spotify(track_id) => self.set_source_spotify(&track_id)?,
        }

        let filter = gst::parse_bin_from_description(video_filter, true)?;
        self.playbin.set_property("video-filter", &filter);

        self.play()
    }

    fn set_source_path(&self, media: &Path) -> Result<()> {
        let path = media.canonicalize()?;
        let uri = Url::from_file_path(path.to_str().unwrap()).unwrap();
        self.playbin.set_property("uri", uri.to_string());

        let mut suburi: Option<String> = None;
        // Assume mp3 file have a CDG associated
        if let Some(ext) = media.extension() {
            if ext == "mp3" {
                let path = path.with_extension("cdg");
                let url = Url::from_file_path(path.to_str().unwrap()).unwrap();
                suburi = Some(url.to_string());
            }
        }
        self.playbin.set_property("suburi", suburi);

        Ok(())
    }
    fn set_source_spotify(&self, track_id: &str) -> Result<()> {
        let track_id = format!("spotify:track:{}", track_id);

        self.playbin.set_property("uri", track_id);
        Ok(())
    }

    pub fn display_message(
        &self,
        message: &str,
        add_overlay_text: bool,
        timer: Option<u64>,
    ) -> Result<()> {
        let mut data_dir = Path::new(config::PKGDATADIR);
        if !data_dir.exists() {
            data_dir = Path::new("data");
        }

        let path = data_dir.join("background.png").canonicalize()?;

        let urls = get_web_urls();
        let txt = if add_overlay_text {
            format!("{}\n{}", message, urls.join("\n"))
        } else {
            message.to_string()
        };
        let txt = gstreamer::glib::markup_escape_text(&txt);

        let qroverlay = if add_overlay_text
            && !urls.is_empty()
            && gst::ElementFactory::find("qroverlay").is_some()
        {
            format!(
                "qroverlay data=\"{}\" y=95 qrcode-error-correction=3 pixel-size=7",
                urls.first().unwrap()
            )
        } else {
            "identity".to_string()
        };

        let desc = format!("textoverlay text=\"{}\" shaded-background=true valignment=center halignment=center name=message ! {} ! imagefreeze", txt, qroverlay);

        self.remove_announce_id();
        self.start_pipeline(Source::Path(path), &desc)?;

        if let Some(timer) = timer {
            // setup timer to notify caller when the message is done being displayed
            let playbin_weak = self.playbin.downgrade();
            let tx = self.tx.clone();
            let announce_id_clone = self.announce_id.clone();

            let source_id = gstreamer::glib::source::timeout_add_seconds(1, move || {
                let mut announce_id = announce_id_clone.lock().unwrap();

                let playbin = match playbin_weak.upgrade() {
                    Some(playbin) => playbin,
                    None => {
                        announce_id.get_mut().take();
                        return gstreamer::glib::ControlFlow::Break;
                    }
                };

                if let Some(pos) = playbin.query_position::<gst::ClockTime>() {
                    if pos.seconds() < timer {
                        return gstreamer::glib::ControlFlow::Continue;
                    }
                }

                trace!("display message done");
                tx.send(Message::DisplayMessageDone).unwrap();

                announce_id.get_mut().take();
                gstreamer::glib::ControlFlow::Break
            });

            let mut announce_id = self.announce_id.lock().unwrap();
            announce_id.get_mut().replace(source_id);
        }

        Ok(())
    }

    pub fn open_song(&self, song: &Song) -> Result<()> {
        debug!("opening {:?}", song);

        let desc = format!(
            "textoverlay text=\"{}\" shaded-background=true valignment=top halignment=right name=ip",
            get_overlay_text()
        );

        self.start_pipeline(Source::from(song), &desc)
    }

    fn set_state(&self, state: gst::State) -> Result<()> {
        debug!("set pipeline to {:?}", state);
        self.playbin.set_state(state)?;
        Ok(())
    }

    pub fn pause(&self) -> Result<()> {
        self.set_state(gst::State::Paused)
    }

    pub fn play(&self) -> Result<()> {
        self.set_state(gst::State::Playing)
    }

    pub fn stop(&self) -> Result<()> {
        debug!("Display background");
        self.display_message(gettext("Waiting for songs").as_str(), true, None)
    }

    pub fn get_position(&self) -> Option<u64> {
        self.playbin
            .query_position::<gst::ClockTime>()
            .map(|pos| pos.seconds())
    }

    fn remove_announce_id(&self) {
        let announce_id = self.announce_id.lock().unwrap();

        if let Some(source_id) = announce_id.take() {
            source_id.remove();
        }
    }

    pub fn restart_song(&self) -> Result<()> {
        // seeking is not working with cdg/mp3 so restart the whole pipeline #yolo
        self.set_state(gst::State::Null)?;
        self.play()?;

        Ok(())
    }
}

impl Drop for Player {
    fn drop(&mut self) {
        self.remove_announce_id();
        self.playbin.set_state(gst::State::Null).unwrap();
    }
}

enum Source {
    Path(PathBuf),
    Spotify(String),
}

impl From<&Song> for Source {
    fn from(song: &Song) -> Self {
        match song {
            Song::Path(path) => Source::Path(path.clone()),
            Song::Db(song) => Source::Path(song.path()),
            Song::Spotify(song) => Source::Spotify(song.track_id.clone()),
        }
    }
}

fn get_pipeline<O: IsA<gst::Object>>(obj: &O) -> Option<gst::Pipeline> {
    let mut parent = obj.as_ref().clone();
    while let Some(new_parent) = parent.parent() {
        parent = new_parent;
    }

    parent.downcast().ok()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::init;
    use crate::tests::TestMedia;
    use gtk::prelude::*;
    use std::{
        sync::atomic::{AtomicBool, Ordering},
        thread, time,
    };

    struct Test {
        player: Player,
        window: gtk::Window,
        done: Arc<AtomicBool>,
    }

    impl Test {
        fn new() -> Test {
            #[allow(deprecated)]
            let (tx, rx) =
                gstreamer::glib::MainContext::channel(gstreamer::glib::Priority::DEFAULT);

            let done = Arc::new(AtomicBool::new(false));
            let done_clone = done.clone();

            rx.attach(None, move |msg| {
                if let Message::PlayingDone = msg {
                    done_clone.store(true, Ordering::Relaxed);
                }
                gstreamer::glib::ControlFlow::Continue
            });

            let window = gtk::Window::new();
            let player = Player::new(tx).unwrap();

            window.set_child(Some(&player.picture));

            let test = Test {
                player,
                window: window.clone(),
                done,
            };

            window.set_size_request(800, 600);
            window.show();

            test
        }

        fn open_media(&mut self, media: TestMedia) {
            self.player.open_song(&Song::Path(media.path())).unwrap();
        }

        fn run_until_eos(&self) {
            let context = gstreamer::glib::MainContext::ref_thread_default();
            while !self.done.load(Ordering::Relaxed) {
                context.iteration(true);
            }
        }
    }

    impl Drop for Test {
        fn drop(&mut self) {
            self.window.destroy();
        }
    }

    #[test]
    #[ignore]
    fn player_tests() {
        let _ = env_logger::try_init();
        init().unwrap();

        let context = gstreamer::glib::MainContext::ref_thread_default();
        let _guard = context.acquire().unwrap();

        play_twice();
        play_next();
        pause();
        stop();
        play_cdg_alone();
        play_video_then_cdg();
        play_cdg_then_video();
        restart_song();
    }

    fn play_twice() {
        println!("play_twice");
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        assert_eq!(test.player.get_position(), None);
        test.run_until_eos();
        assert_eq!(test.player.get_position(), Some(2));

        test.open_media(TestMedia::Video2s);
        test.run_until_eos();
    }

    fn play_next() {
        println!("play_next");
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        thread::sleep(time::Duration::from_millis(500));

        test.open_media(TestMedia::Video2_2s);
        test.run_until_eos();
    }

    fn pause() {
        println!("pause");
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        thread::sleep(time::Duration::from_millis(500));

        test.player.pause().unwrap();
        thread::sleep(time::Duration::from_millis(500));
        test.player.play().unwrap();
        test.run_until_eos();
    }

    fn stop() {
        println!("stop");
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        test.player.stop().unwrap();
    }

    fn play_cdg_alone() {
        println!("play_cdg_alone");
        let mut test = Test::new();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();
    }

    fn play_video_then_cdg() {
        println!("play_video_then_cdg");
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        test.run_until_eos();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();
    }

    fn play_cdg_then_video() {
        println!("play_cdg_then_video");
        let mut test = Test::new();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();

        test.open_media(TestMedia::Video2s);
        test.run_until_eos();
    }

    fn restart_song() {
        println!("restart_song");
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        thread::sleep(time::Duration::from_millis(500));

        test.player.restart_song().unwrap();
        thread::sleep(time::Duration::from_millis(500));
        test.player.play().unwrap();
        test.run_until_eos();
    }
}
