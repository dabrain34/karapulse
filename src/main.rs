// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

#[macro_use]
extern crate log;

use anyhow::Result;
use chrono::Duration;
use env_logger::{Builder, Env};
use gettextrs::*;
use gio::prelude::*;
use gstreamer as gst;
use gtk::prelude::*;
use std::cell::RefCell;
use std::path::PathBuf;
use structopt::StructOpt;

mod common;
mod config;
mod db;
mod karapulse;
mod player;
mod protocol;
mod queue;
mod settings;
mod spotify;
/// FIXME: remove once the warning has been fixed in Rocket
#[allow(clippy::let_unit_value)]
mod web;

#[cfg(test)]
mod tests;

use crate::common::init;
use crate::db::DB;
use crate::karapulse::{Karapulse, Song};
use crate::web::start_web;
use config::{GETTEXT_PACKAGE, LOCALEDIR};

#[derive(StructOpt, Debug)]
#[structopt(name = "karapulse")]
struct Opt {
    #[structopt(
        long = "config",
        short = "c",
        parse(from_os_str),
        help = "Configuration file"
    )]
    config_file: Option<PathBuf>,
    #[structopt(long = "local-files", parse(from_os_str), help = "Local files to play")]
    local_files: Vec<PathBuf>,
    #[structopt(long = "db-songs", help = "Songs ID from the DB to play")]
    db_songs: Vec<i64>,
    #[structopt(long = "spotify-songs", help = "Spotify track ID to play")]
    spotify_songs: Vec<String>,
    #[structopt(name = "db", long = "db-path")]
    db_path: Option<PathBuf>,
    #[structopt(short = "r", long = "restore", help = "Restore queue")]
    restore: bool,
}

async fn restore_queue(karapulse: &Karapulse, db: &DB, spotify: &Option<spotify::Spotify>) {
    let history = db.history_not_played().unwrap();
    let last = match history.last() {
        Some(h) => h.queued,
        None => return,
    };
    info!("restoring queue from {}", last);

    for h in history {
        if last - h.queued > Duration::days(1) {
            trace!("too old, skip {:?}", h);
        } else {
            debug!("restore {:?}", h);

            match karapulse::Song::from_history_song(h.song, spotify).await {
                Ok(song) => karapulse.enqueue(&h.user, song, Some(h.rowid)).unwrap(),
                Err(e) => {
                    warn!("failed to restore: {}", e)
                }
            }
        }
    }
}

#[rocket::main]
async fn main() -> Result<()> {
    Builder::from_env(Env::default().default_filter_or("warn")).init();
    init()?;

    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR)?;
    textdomain(GETTEXT_PACKAGE)?;

    let opt = Opt::from_args();

    let settings_path = opt
        .config_file
        .clone()
        .unwrap_or_else(|| crate::common::config_dir_file("config.toml").unwrap());
    let settings = settings::Settings::from_path(&settings_path)?;

    let spotify = match spotify::Spotify::new().await {
        Ok(spotify) => {
            info!("successfully logged to Spotify");
            Some(spotify)
        }
        Err(e) => {
            warn!("failed to connect to Spotify: {}", e);
            None
        }
    };

    let mut spotify_songs = Vec::new();
    if !opt.spotify_songs.is_empty() {
        if let Some(spotify) = spotify.as_ref() {
            for id in opt.spotify_songs.iter() {
                match spotify.song_from_id(id).await {
                    Ok(song) => spotify_songs.push(Song::Spotify(song)),
                    Err(err) => error!("failed to retrieve Spotify song {}: {:?}", id, err),
                }
            }
        }
    }

    let application = gtk::Application::new(
        Some(config::APP_ID),
        gio::ApplicationFlags::HANDLES_COMMAND_LINE,
    );

    gstreamer::glib::set_application_name(&format!("Karapulse{}", config::NAME_SUFFIX));

    application.connect_command_line(|_app, _cmd_line| {
        // structopt already handled arguments
        0
    });

    application.connect_startup(move |app| {
        #[allow(deprecated)]
        let (tx, rx) = gstreamer::glib::MainContext::channel(gstreamer::glib::Priority::DEFAULT);
        let db = DB::new(&opt.db_path).unwrap();

        let window = gtk::ApplicationWindow::new(app);

        app.inhibit(
            Some(&window),
            gtk::ApplicationInhibitFlags::IDLE,
            Some("Karapulse running"),
        );

        let window = window.upcast::<gtk::Window>();
        let karapulse = Karapulse::new(window, tx.clone(), rx, db);

        let db = DB::new(&opt.db_path).unwrap();
        if opt.restore {
            futures::executor::block_on(async { restore_queue(&karapulse, &db, &spotify).await });
        }

        for media in opt.local_files.clone() {
            info!("queuing {}", media.display());
            /* FIXME: fetch info from file */
            karapulse.enqueue("CLI", Song::Path(media), None).unwrap();
        }

        for id in opt.db_songs.iter() {
            match db.find_song(*id) {
                Ok(song) => karapulse.enqueue("CLI", Song::Db(song), None).unwrap(),
                Err(err) => error!("failed to queue DB song {}: {:?}", id, err),
            }
        }

        for song in spotify_songs.clone() {
            karapulse.enqueue("CLI", song, None).unwrap();
        }

        karapulse.play().unwrap();

        let spotify_clone = spotify.clone();
        let settings_clone = settings.clone();

        /* Start web server in its own thread*/
        tokio::spawn(async move {
            start_web(settings_clone, tx, db, spotify_clone)
                .await
                .unwrap();
        });

        app.connect_activate(move |_app| {
            // TODO: present window
        });

        // Pass ownership of karapulse to the cell and the cell
        // to the callback so karapulse stays alive until shutdown
        let container = RefCell::new(Some(karapulse));
        app.connect_shutdown(move |_| {
            let _karapulse = container
                .borrow_mut()
                .take()
                .expect("Shutdown called multiple times");
        });
    });

    application.run();

    debug!("exiting");
    unsafe {
        gst::deinit();
    }
    Ok(())
}
