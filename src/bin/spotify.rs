use anyhow::Result;
use env_logger::{Builder, Env};

use karapulse::spotify::Spotify;

#[tokio::main]
async fn main() -> Result<()> {
    Builder::from_env(Env::default().default_filter_or("warn")).init();

    let spotify = Spotify::new().await?;
    let songs = spotify.search("nirvana lithium").await?;
    dbg!(&songs);

    Ok(())
}
