import {BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {HttpClientModule} from '@angular/common/http';
import {PlayerControlComponent} from './playercontrol/playercontrol.component';
import {SearchResultsComponent} from './searchresults/searchresults.component';
import {SearchBarComponent} from './searchbar/searchbar.component';
import {ProfileComponent} from './profile/profile.component';
import {DashboardComponent} from './dashboard/dashboard.component';

import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';

import {MatTableModule} from '@angular/material/table';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SearchComponent} from './search/search.component';
import {SongDetailsComponent} from './song-details/song-details.component';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {FirstRunDialogComponent, NameDialogComponent} from './first-run-dialog/first-run-dialog.component';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {QueueComponent} from './queue/queue.component';
import {QueueEntryComponent} from './queue-entry/queue-entry.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {HistoryComponent} from './history/history.component';
import { AutofocusDirective } from './autofocus.directive';
import { TrimmedMinLengthValidatorDirective } from './helpers/trimmed-min-length-validator.directive';

import { QRCodeModule } from 'angularx-qrcode';
import {QRCodeComponent} from './qrcode/qrcode.component';
import { ConfigPageComponent } from './config-page/config-page.component';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import { ConfigPageLoginComponent } from './config-page-login/config-page-login.component';


@NgModule({
  declarations: [
    AppComponent,
    PlayerControlComponent,
    SearchResultsComponent,
    SearchBarComponent,
    ProfileComponent,
    DashboardComponent,
    SearchComponent,
    SongDetailsComponent,
    FirstRunDialogComponent,
    NameDialogComponent,
    QueueComponent,
    QueueEntryComponent,
    HistoryComponent,
    AutofocusDirective,
    TrimmedMinLengthValidatorDirective,
    QRCodeComponent,
    ConfigPageComponent,
    ConfigPageLoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    FormsModule,
    MatTableModule,
    MatListModule,
    MatBottomSheetModule,
    MatDialogModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    QRCodeModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    /*HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, {dataEncapsulation: false}),*/
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}, {
    provide: HAMMER_GESTURE_CONFIG, useClass: HammerGestureConfig
  },
  ],
  bootstrap: [AppComponent],
  entryComponents: [SongDetailsComponent, NameDialogComponent, QueueEntryComponent, FirstRunDialogComponent],
})
export class AppModule {
}
