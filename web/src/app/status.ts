import {Song} from './song';


export type CurrentSong = {
  user: string;
  song: Song;
}

export type Status = {
  state: State;
  // tslint:disable-next-line:variable-name
  current_song: CurrentSong;
  queue: QueueEntry[];
  position?: number;
}

export enum State {
  PLAYING = 'Playing',
  WAITING = 'Waiting',
  PAUSED = 'Paused',
}

export type QueueEntry = {
  user: string;
  song: Song;
  eta: number;
}

export type PlayHistory = QueueEntry[];
