import {Directive, Input} from '@angular/core';


import {AbstractControl, NG_VALIDATORS, ValidationErrors, ValidatorFn} from "@angular/forms";

/** Length should be bigger than minimum, after we have trimmed spaces */
export function trimmedMinLength(minLength: number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const trimmedName = control.value ? control.value.trim() : ''
    return trimmedName.length  >=  minLength ? null : {minlength: {valid: false}};
  };
}

@Directive({
  selector: '[appTrimmedMinLength]',
  providers: [{provide: NG_VALIDATORS, useExisting: TrimmedMinLengthValidatorDirective, multi: true}]
})
export class TrimmedMinLengthValidatorDirective {
  @Input('appTrimmedMinLength') minLength:number|string = 0;

  validate(control: AbstractControl): ValidationErrors | null {
    const minLength = Number(this.minLength);
    const tmp = minLength ? trimmedMinLength(minLength)(control)
      : null;
    return tmp;
  }
}


