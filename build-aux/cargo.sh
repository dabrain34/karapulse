#!/bin/bash

export MESON_BUILD_ROOT="$1"
export MESON_SOURCE_ROOT="$2"
export CARGO_TARGET_DIR="$MESON_BUILD_ROOT"/target
export OUTPUT="$3"
export EXTRA_ARGS="$4"
export BIN="$5"
export OFFLINE="$6"

cargo build $OFFLINE --bins --tests --manifest-path \
    "$MESON_SOURCE_ROOT"/Cargo.toml --release $EXTRA_ARGS && \
    cp "$CARGO_TARGET_DIR"/release/$BIN $OUTPUT